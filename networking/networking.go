package networking

import (
	"encoding/json"
	"net/http"
	"os"
)

// EnableCors enables CORS on http methods.
func EnableCors(w *http.ResponseWriter) {
	switch os.Getenv("SERVER_ENVIRONMENT") {
	case "test":
		fallthrough
	case "dev":
		(*w).Header().Set("Access-Control-Allow-Origin", "http://localhost:32395")
	case "prod":
		(*w).Header().Set("Access-Control-Allow-Origin", "https://status.soratech.com")
	}
	(*w).Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE, PATCH")
	(*w).Header().Set("Access-Control-Allow-Credentials", "true")
}

// WriteJSON marshals the given Site struct into JSON and sends that JSON as an HTTP Response
func WriteJSON(w http.ResponseWriter, i interface{}) {
	u, err := json.Marshal(i)
	if err != nil {
		panic(err)
	}
	w.Header().Add("Content-Type", "application/json; charset=utf-8")
	w.Write(u)
}
