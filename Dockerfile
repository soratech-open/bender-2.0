FROM alpine:latest

COPY bender /var/run/bender

ENTRYPOINT [ "/var/run/bender" ]