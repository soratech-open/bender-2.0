package slack

import (
	"bytes"
	"crypto/tls"
	"encoding/json"
	"fmt"
	"math/rand"
	"net/http"
	"strings"
	"time"

	"gitlab.com/soratech/Gamification/bender-2.0/secrets"
)

// Request represents the data sent from Slack's Event API.
type Request struct {
	Token       string   `json:"token"`
	Type        string   `json:"type"`
	TeamID      string   `json:"team_id,omitempty"`
	APIAppID    string   `json:"api_app_id,omitempty"`
	Event       event    `json:"event,omitempty"`
	AuthedUsers []string `json:"authed_users,omitempty"`
	EventID     string   `json:"event_id,omitempty"`
	EventTime   int      `json:"event_time,omitempty"`
	Challenge   string   `json:"challenge,omitempty"`
}

type event struct {
	Channel         string `json:"channel"`
	ChannelType     string `json:"channel_type"`
	ClientMessageID string `json:"client_msg_id"`
	EventTimestamp  string `json:"event_ts"`
	Text            string `json:"text"`
	Timestamp       string `json:"ts"`
	Type            string `json:"type"`
	User            string `json:"user"`
	Subtype         string `json:"subtype,omitempty"`
}

type response struct {
	Channel     string      `json:"channel"`
	Text        string      `json:"text"`
	AsUser      bool        `json:"as_user,omitempty"`
	Attachments interface{} `json:"attachments,omitempty"`
	Blocks      interface{} `json:"blocks,omitempty"`
	IconEmoji   string      `json:"icon_emoji,omitempty"`
	IconURL     string      `json:"icon_url,omitempty"`
	LinkNames   bool        `json:"link_names,omitempty"`
	Markdown    bool        `json:"mrkdwn"`
}

// sendMessage sends messages to Slack
func sendMessage(data response) {
	url := "https://slack.com/api/chat.postMessage"
	tr := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
	}

	secret, err := secrets.GetSecrets()

	if err != nil {
		panic(err)
	}

	message, err := json.Marshal(data)

	if err != nil {
		panic(err)
	}

	request, err := http.NewRequest("POST", url, bytes.NewBuffer(message))

	if err != nil {
		panic(err)
	}

	request.Header.Set("Content-Type", "application/json")
	request.Header.Set("Authorization", fmt.Sprintf("Bearer %s", secret.OauthToken))

	client := &http.Client{Transport: tr}

	response, err := client.Do(request)

	if err != nil {
		panic(err)
	}

	defer response.Body.Close()
	return
}

// HandleEvent handles requests from the Events API.
func HandleEvent(event Request) {
	if event.Event.Subtype != "bot_message" {
		var message response
		var responseText string
		text := strings.ToLower(event.Event.Text)

		switch {
		case strings.Contains(text, "help"):
			responseText = helper()
		case strings.Contains(text, "office password"):
			fallthrough
		case strings.Contains(text, "guest password"):
			fallthrough
		case strings.Contains(text, "bta password"):
			fallthrough
		case strings.Contains(text, "statusboard password"):
			fallthrough
		case strings.Contains(text, "loc password"):
			responseText = passwordHandler(text)
		case strings.Contains(text, "eight ball") || strings.Contains(text, "8 ball"):
			responseText = eightBall()
		case strings.Contains(text, "support phone"):
			responseText = "(309) 693-7672"
		case strings.Contains(text, "support email"):
			responseText = "support@soratech.com"
		case strings.Contains(text, "make me a sandwich"):
			if strings.Contains(text, "sudo") {
				responseText = ":sandwich:"
			} else {
				responseText = "https://morbotron.com/gif/S05E15/586319/588187.gif?b64lines=Qml0ZSBteSBzaGlueSBtZXRhbCBhc3M="
			}
		case strings.Contains(text, "chuck norris"):
			responseText = chuckNorris()
		case strings.Contains(text, "quote"):
			responseText = quoteGifs()
		case strings.Contains(text, "the rules"):
			responseText = "You think robots care what some hack science-fiction writer thinks? I killed Isaac Asimov on the way over here. Well, Isaac _somebody_."
		case strings.Contains(text, "flip a coin"):
			responseText = coinFlip()
		default:
		}

		if responseText != "" {
			switch {
			case event.Event.ChannelType == "im":
				//someone directly messaged bender.
				message = response{
					Channel:  event.Event.Channel,
					Text:     responseText,
					Markdown: true,
				}
				sendMessage(message)
				return
			case event.Event.Type == "app_mention":
				// someone @-ed bender.
				message = response{
					Channel:  event.Event.Channel,
					Text:     fmt.Sprintf("<@%s> %s", event.Event.User, responseText),
					Markdown: true,
				}
				sendMessage(message)
				return
			}
		}
	}
}

func passwordHandler(text string) string {
	secret, err := secrets.GetSecrets()
	var responseContent string
	if err != nil {
		panic(err)
	}

	switch {
	case strings.Contains(text, "office password"):
		responseContent = fmt.Sprintf("The office wifi password is: `%s`", secret.Sora.OfficeWifi)
	case strings.Contains(text, "guest password"):
		responseContent = fmt.Sprintf("The guest wifi password is: `%s`", secret.Sora.GuestWifi)
	case strings.Contains(text, "bta password"):
		responseContent = fmt.Sprintf("The BTA account password is: `%s`", secret.Sora.BTAPassword)
	case strings.Contains(text, "loc password"):
		responseContent = fmt.Sprintf("The LOC account password is: `%s`", secret.Sora.LOCPassword)
	case strings.Contains(text, "statusboard password"):
		responseContent = fmt.Sprintf("The statusboard account password is: `%s`", secret.Sora.StatusboardPassword)
	}

	return responseContent
}

func helper() string {
	list := `
	*bta password* : Returns the BTA account password
*chuck norris* : Returns a fact about Chuck Norris
*eight ball* or *8 ball* : Ask the magic eight ball a question
*flip a coin* : Bender flips a coin
*guest password* : Returns the guest wifi password
*help* : Returns this list
*loc password* : Returns the LOC account password
*make me a sandwich* : Ask bender to make you a sandwich
*office password* : Returns the office wifi password
*quote* : Returns a Bender quote and a GIF
*statusboard password* : Returns the statusboard password
*support email* : Returns the support email address
*support phone* : Returns the support phone number
*the rules* : Bender tells you what he thinks of Asimov's laws of robotics
`

	return list
}

func eightBall() string {
	responses := []string{
		"It is certain.",
		"It is decidedly so.",
		"Without a doubt.",
		"Yes - definitely.",
		"You may rely on it.",
		"As I see it, yes.",
		"Most Likely.",
		"Outlook Good.",
		"Yes",
		"Signs point to yes",
		"Reply hazy, try again",
		"Ask again later",
		"Better not tell you now",
		"Cannot predict now",
		"Concentrate and ask again",
		"Don't count on it",
		"My reply is no",
		"My sources say no",
		"Outlook not so good",
		"Very doubtful",
	}
	rand.Seed(time.Now().Unix())
	return fmt.Sprintf(":8ball: : %s", responses[rand.Intn(len(responses))])
}

func quoteGifs() string {
	quotes := []string{
		"Well, all right. But I don't want anyone to think we're robosexual or anything, so if anyone asks, you're my debugger. \n https://morbotron.com/gif/S01E01/680404/684158.gif?b64lines=",
		"https://morbotron.com/gif/S05E15/586319/588187.gif?b64lines=Qml0ZSBteSBzaGlueSBtZXRhbCBhc3M=",
		"Dream on, skintube! I'm only programmed to bend for constructive purposes. What do I look like, a debender?\n https://morbotron.com/gif/S01E01/848288/854695.gif?b64lines=",
		"I'm the first one to work. A new low.\n https://morbotron.com/gif/S04E04/213512/219351.gif?b64lines=",
		"So what could have caused that leak? A heat fracture, on account of I'm so hot?\n https://morbotron.com/gif/S07E06/330330/336377.gif?b64lines=",
		"Nothing like a warm fire and a super soaker of fine cognac.\n https://morbotron.com/gif/S02E08/259919/265758.gif?b64lines=",
		"O' cruel fate, to be thusly boned! Ask not for whom the bone bones - it bones for thee\n https://morbotron.com/gif/S04E05/222938/229612.gif?b64lines=",
		"You're screwier than my Aunt Rita, and she's a screw.\n https://morbotron.com/gif/S05E09/927325/931296.gif?b64lines=",
		"Bender must be stopped! I have gone too far. Who does that guy think I am?\n https://morbotron.com/gif/S05E15/1088320/1091440.gif?b64lines=",
		"Tempers are wearing thin. Let's just hope some robot doesn't kill everyone.\n https://morbotron.com/gif/S02E16/500869/505457.gif?b64lines=",
		"My story is a lot like yours, only more interesting 'cause it involves robots\n https://morbotron.com/gif/S04E09/587569/591740.gif?b64lines=",
		"I'll make my own Slack Team. With blackjack and hookers! In fact, forget the Slack Team!\n https://morbotron.com/gif/S01E02/865207/873131.gif?b64lines=",
		"The world must learn of our peaceful ways...by force!\n https://morbotron.com/gif/S05E14/900299/905304.gif?b64lines=",
		"I don't tell you how to tell me what to do, so don't tell me how to do what you tell me to do.\n https://morbotron.com/gif/S06E01/1697030/1702870.gif?b64lines=",
		"I'm sober, and I don't know what I might do\n https://morbotron.com/gif/S01E03/1153911/1157248.gif?b64lines=",
		"As a robot, I can't feel human emotions, and sometimes that makes me sad.\n https://morbotron.com/gif/S04E08/60676/63396.gif?b64lines=",
		"Computer dating. It's like pimping, except you rarely have to use the phrase \"upside your head\"\n https://morbotron.com/gif/S02E11/535179/541853.gif?b64lines=",
	}
	rand.Seed(time.Now().Unix())
	return quotes[rand.Intn(len(quotes))]
}

func getInsult() string {
	insults := []string{
		"meatbag",
		"skintube",
		"chump",
		"chumpette",
		"skinbag",
		"organ-sack",
		"fleshwad",
		"coffin-stuffer",
	}
	rand.Seed(time.Now().Unix())
	return insults[rand.Intn(len(insults))]
}

func chuckNorris() string {
	facts := []string{
		"Chuck Norris can split an atom -- and put it back together.",
		"Chuck Norris doesn't fear the Reaper. The Reaper fears Chuck Norris.",
		"Chuck Norris's phone number is infinity.",
		"An apple a day doesn't keep the doctor away. Chuck Norris keeps the doctor away.",
		"Chuck Norris doesn't lose the game, but you just did.",
		"Chuck Norris can change a tire on a moving car.",
		"Chuck Norris can use a touchscreen without touching it.",
		"Chuck Norris went up the creek without a paddle--or a canoe.",
		"Chuck Norris doesn't need oxygen. Oxygen needs Chuck Norris.",
		"Chuck Norris doesn't need a shirt or shoes for service.",
		"Chuck Norris can break the sound barrier with his fist.",
		"Chuck Norris doesn't wait in line; the line waits for Chuck Norris",
		"You don't have to look for Chuck Norris. He is already there.",
		"Chuck Norris roundhouse kicked the universe. That was the Big Bang.",
		"Money doesn't grow on trees, but it does grow on Chuck Norris.",
		"The theory of relativity is relative only to how Chuck Norris interprets it.",
		"The Pope once tried to bless Chuck Norris, but nobody crosses Chuck Norris",
		"Guns sleep with a picture of Chuck Norris under their pillow",
		"Chuck Norris can blow bubbles with beef jerky.",
		"To sharks, Chuck Norris Week is the scariest week on television.",
		"Nobody limits Chuck Norris to 140 characters.",
		"Chuck Norris invented napalm when he needed some new aftershave.",
		"Chuck Norris can jump into the air--and stay there.",
		"When Chuck Norris wants to take a nap, he turns off the sun.",
		"Chuck Norris's beard clippings are used by NASA to insulate rocket engines.",
		"When Chuck Norris has a good idea, a light bulb doesn't appear over his head; when Chuck Norris has a good idea, a star explodes.",
		"When Chuck Norris walks barefoot on his wood deck, the deck gets Chuck Norris splinters.",
		"Pluto was Earth's moon once, but it got in Chuck Norris's way and he roundhouse kicked it to the end of the solar system.",
		"When Chuck Norris was little, he didn't sleep with a teddy bear. He slept with an actual bear.",
		"When Chuck Norris says his own name, an earthquake occurs.",
		"Chuck Norris died at his home this morning in Texas. He has since recovered from this minor inconvenience and is reported to be doing well.",
		"If Chuck Norris's beard is wet and he squeezes it, lava comes out.",
		"Chuck Norris's computer doesn't have a backspace key. It doesn't need one, because Chuck Norris never makes mistakes.",
		"Chuck Norris can talk to his echo",
		"If a farmer puts a picture of Chuck Norris in his field, the birds will bring back all the grain they stole from it last year.",
		"Chuck Norris punched water -- and left a dent.",
		"Chuck Norris is NASA's plan b. If a rocket fails to launch, he kicks it into space.",
		"Chuck Norris can eat soup with a fork faster than you can beg for mercy.",
		"Solar eclipses happen when Chuck Norris has a staring contest with the sun. Chuck Norris wins.",
		"Chuck Norris boxed his own shadow--and won.",
		"Chuck Norris invented the circle with his roundhouse kick.",
		"Big Brother isn't watching you. Chuck Norris is watching you.",
		"The climate requires Chuck Norris's permission to change.",
		"Light was just light until Chuck Norris roundhouse kicked it, then it became the visible light spectrum.",
		"When Chuck Norris needs a diamond, he kicks a lump of coal and it turns into one.",
		"If Chuck Norris votes for you, you win. No matter what.",
		"When Chuck Norris rides the rail, he arrives before the train.",
		"Chuck Norris kicked Jack Frost so hard that Spring came.",
		"Chuck Norris is why tumbleweeds tumble.",
		"Chuck Norris killed two stones with one bird.",
		"When Chuck Norris goes to chop wood, he doesn't bring an axe. He brings his beard.",
		"When Chuck Norris was pitching, the umpire got a blast wave concussion.",
		"Chuck Norris can find two snowflakes that are exactly alike. Chuck Norris can find anything.",
		"Chuck Norris can make a three-sided square.",
		"Chuck Norris is so fast his GPS system talks to him in the past tense.",
		"Some people can juggle chainsaws. Chuck Norris can juggle people who are juggling chainsaws.",
		"Chuck Norris doesn't use toothpaste. He uses concrete.",
		"The First Law of Thermodynamics states that matter cannot be created nor destroyed--unless it meets Chuck Norris.",
		"Wash Chuck Norris' tee shirt carefully, or it will beat you up.",
		"If you plant magic beans and climb the beanstalk, you will find Chuck Norris. He already killed the giant.",
		"Chuck Norris uses pepper spray to spice up his steak.",
		"If looks could kill, they would be called Chuck Norris.",
		"If Chuck Norris were to hit himself, he would create an infinite loop by countering each blow, a paradox that no one can solve--except Chuck Norris.",
		"Chuck Norris doesn't cheat death. He beats it fair and square.",
		"Chuck Norris likes to relax in a hot bath--a lava bath.",
		"Even Chuck Norris's teeth have muscles.",
		"You win some, you lose some. Chuck Norris wins some, and then wins some more.",
		"Chuck Norris created the moon because he needed a nightlight.",
		"Chuck Norris doesn't need to use apostrophes to let you know what's his. It's all his.",
		"Chuck Norris uses a stunt double for crying scenes.",
		"Chuck Norris doesn't pay taxes. The government pays taxes to Chuck Norris.",
		"Drones are afraid of Chuck Norris strikes.",
		"When Chuck Norris goes to the rodeo, bulls ride him.",
		"Chuck Norris can shuffle a deck of phone books.",
		"Chuck Norris was born in a log cabin that he built.",
		"The only way Chuck Norris catches a cold is in a headlock.",
		"Chuck Norris can form a straight circle.",
		"When Chuck Norris swings a sword, the air bleeds.",
		"Chuck Norris can lick his own elbow with both arms tied behind his back.",
		"Chuck Norris doesn't have to face the consequences; the consequences have to face Chuck Norris.",
		"The universe is expanding because Chuck Norris needs room to flex his muscles.",
		"Chuck Norris keeps his friends close and his enemies closer--close enough to drop them with a roundhouse kick to the face.",
		"When Chuck Norris flexes his muscles, your shirt rips.",
		"Every four years is a leap year, but only after Chuck Norris tells the year to leap.",
		"When Chuck Norris forgets something, it ceases to exist.",
		"Chuck Norris grinds his coffee with his teeth and then boils it with his rage.",
		"When Chuck Norris looks at Medusa, he doesn't turn to stone--Medusa does.",
		"When Chuck Norris jumps out of a plane, he survives; gravity is just too scared to let him fall.",
		"Chuck Norris once ran a marathon backwards just to see what second place looked like.",
		"When Chuck Norris gets a pizza, the delivery boy tips him.",
		"When Chuck Norris was born, the doctor cried. Nobody slaps Chuck Norris",
		"Chuck Norris died once, but he got over it.",
		"When Chuck Norris stands on the beach, the tide is afraid to come in.",
		"Chuck Norris is both a rock and a hard place.",
		"Chuck Norris can see all 50 states from his house.",
		"Chuck Norris uses a nightlight. Not because he's afraid of the dark, but because the dark is afraid of Chuck Norris.",
		"Chuck Norris swam the English Channel in 12 languages.",
		"Chuck Norris actually _can_ slap you into next week",
		"When Chuck Norris went to Paris, the Eiffel Tower had its picture taken with him.",
		"Chuck Norris doesn't make a splash when he jumps into water; the water gets out of his way.",
		"Global Warming is caused by Chuck Norris's body heat.",
		"Chuck Norris can put toothpaste back into the tube.",
		"Chuck Norris can shoot three-pointers with a bowling ball.",
		"When eggs hear Chuck Norris coming, they scramble themselves.",
		"Did you hear that? Chuck Norris just got a little more awesome.",
		"Chuck Norris can create tornadoes by running around in circles.",
		"Chuck Norris doesn't go for a run; he goes for a chase.",
		"When you look at a picture of Chuck Norris online, he can see you, too -- and he's waiting for you to apologize.",
		"Chuck Norris doesn't turn on the shower. He looks at it until it cries.",
		"Chuck Norris can follow you even if he's ahead of you.",
		"Chuck Norris fell off a cliff and landed at the top.",
		"Chuck Norris can put 13 eggs in a carton made for a dozen.",
		"Chuck Norris can hit a grand slam when the bases are empty.",
		"Chuck Norris can put out a fire with gasoline.",
		"If a tree falls in the forest, chances are Chuck Norris leaned on it.",
		"Chuck Norris can do a flip without moving.",
		"If Chuck Norris were a chameleon, he wouldn't blend into the background. The background would blend into him.",
		"Chuck Norris can punch a cyclops between the eyes.",
		"Chuck Norris scares the needle of of haystacks",
		"Chuck Norris went out for a swim and had to tow a cruise ship back to port.",
		"When Chuck Norris is late, time has to slow down.",
		"Chuck Norris can win at blackjack with just one card.",
		"If you stab Chuck Norris, your knife will bleed, then you will bleed.",
		"Chuck Norris shaves with his nails; they're the only thing that can penetrate his beard. But the moustache stays.",
		"When Chuck Norris has a heart attack, he attacks back.",
		"Chuck Norris invented math when he started counting.",
		"They were going to put Chuck Norris's face on Mount Rushmore, but the granite wasn't strong enough to hold his beard.",
		"Chuck Norris doesn't need a TV remote. He stares at the television until it changes the channel.",
		"The grass is always greener on Chuck Norris's lawn -- always.",
		"Chuck Norris can mix oil and water by just staring at it.",
		"Chuck Norris is the reason Jack is in the box.",
		"You can't shoot Chuck Norris. If you try, the bullets all run away.",
		"Chuck Norris can put Humpty Dumpty back together again.",
		"Chuck Norris doesn't beat around the bush. He beats up the bush.",
		"When Chuck Norris swims in the ocean, the sharks head for land.",
		"When the Boogeyman goes to bed at night, he checks his closet for Chuck Norris.",
		"Chuck Norris can staple water to a tree.",
		"Chuck Norris puts ice cube trays filled with water in the cupboard, and he gets ice.",
		"Chuck Norris can grill an ice-cream sandwich. It doesn't dare melt.",
		"When Chuck Norris says it's hot, people sweat.",
		"Chuck Norris takes his temperature using a Geiger counter.",
		"A black hole is created when Chuck Norris roundhouse kicks a star.",
		"Blood faints at the sight of Chuck Norris.",
		"Chuck Norris won a guitar battle with a violin.",
		"Chuck Norris can tap dance in flip-flops.",
		"Chuck Norris can speak Spanish in three different languages.",
		"Chuck Norris can make the cows come home.",
		"Chuck Norris can head-butt himself in the face.",
		"Chuck Norris is so humble he brags about it.",
		"Chuck Norris never applies for patents. Companies just know to pay him royalties.",
		"When Chuck Norris goes to the beach, the tide goes out.",
		"Chuck Norris watched the first steps on the moon--from his summer home on Mars.",
		"When Chuck Norris puts his phone on airplane mode, it becomes an airplane.",
		"The Earth doesn't revolve around the sun. It revolves around Chuck Norris.",
		"Chuck Norris can fight fire with his fists.",
		"When a tank shoots Chuck Norris, the tank explodes.",
		"Chuck Norris showers every morning--in a meteor shower.",
		"Chuck Norris's motorcycle has four-wheel drive.",
		"Chuck Norris is a universal translator even though he speaks only one language.",
		"Chuck Norris's fists make the speed of sound wish it were faster.",
		"Chuck Norris doesn't throw up. Chuck Norris throws down.",
		"Why does the sun rise in the East? Because Chuck Norris is chasing it from the other side.",
		"Chuck Norris can hold his breath underwater--for as long as there will be water.",
		"What time is it when Chuck Norris knocks on your door? Time to run.",
		"The belt that holds up Chuck Norris's pants has a black belt in Chun Kuk Do.",
		"Chuck Norris ran faster than the speed of light and went back in time.",
		"When Chuck Norris goes fishing, he stands on the edge of the water and says \" Don't make me come in there and get you\"",
		"When Chuck Norris lifts weights, the weights get stronger.",
		"Chuck Norris doesn't have a reflection; there is only one Chuck Norris.",
		"Chuck Norris can spit fire underwater.",
		"Chuck Norris is immortal because death fears him.",
		"Chuck Norris can email a roundhouse kick.",
		"Cavemen didn't discover fire. Chuck Norris did.",
		"Chuck Norris must like you. If he didn't, you'd already be dead.",
		"Chuck Norris is the only person who can kick you in the back of the face.",
		"Chuck Norris doesn't need to use postage stamps. He just tells the letter where to go and it gets there.",
		"Chuck Norris can convert kilograms into centimeters.",
		"Chuck Norris doesn't pick apples. They jump off the tree into his basket.",
		"When Chuck Norris goes on safari, the animals have to stay in their cars.",
		"Chuck Norris has a shadow, but it's too afraid of Chuck to show itself.",
		"Chuck Norris doesn't bungee jump; he pulls the ground in his direction.",
		"Chuck Norris once won a drag race on a unicycle.",
		"140 characters are not enough to describe the awesomeness of Chuck Norris.",
		"Chuck Norris can climb Mount Everest from top to bottom.",
		"Chuck Norris doesn't step on toes. He steps on necks.",
		"Shooting stars make a wish when they see Chuck Norris.",
		"You can resize a photo of a landscape. Chuck Norris can resize the landscape.",
		"Chuck Norris can write a term paper without any vowels and receive an honorary doctorate for it.",
		"Chuck Norris inflated a flat school bus tire with his lungs.",
		"Chuck Norris's beard can sense danger.",
		"Chuck Norris doesn't have a star on Hollywood Boulevard; he has a constellation.",
		"Chuck Norris once broke the sound barrier. In half.",
		"The square root of two is not irrational; it just hasn't recovered from a Chuck Norris roundhouse kick yet.",
		"Chuck Norris has a bearskin rug in his living room. The bear isn't dead -- it just doesn't dare move.",
		"The meaning of life is in the palm of Chuck Norris's hand--but Chuck Norris never unclenches his fist.",
		"Chuck Norris won a staring contest with a statue.",
		"Chuck Norris laughs in the face of danger--then roundhouse kicks it to death.",
		"When Chuck Norris played the card game War with a friend, France surrendered.",
		"Everybody knows you don't mess with Texas. And even Texas knows you don't mess with Chuck Norris.",
		"When Chuck Norris holds his breath, time stands still.",
		"Chuck Norris is always ahead of everybody else. During daylight savings time, he's two hours ahead.",
		"The Earth doesn't spin on it's own. It spins when Chuck Norris runs westward.",
		"Chuck Norris doesn't lie. Anything he says immediately becomes true.",
		"Chuck Norris calls 911 to see if he can help out.",
		"Chuck Norris oversleeps sometimes because his alarm clock is too scared to wake him.",
		"Chuck Norris casts a shadow even in total darkness.",
		"Chuck Norris can't be a referee because the only penalty he knows is the death penalty.",
		"Chuck Norris spent a month in El Paso one night.",
		"The Devil went down to Georgia--and Chuck Norris kicked him out.",
		"Chuck Norris can win a hotdog eating contest without eating any hotdogs.",
		"Chuck Norris assisted the doctor at his own birth.",
		"If Chuck Norris is angry with you, he doesn't knock you into next week. He knocks you into a parallel universe.",
		"Chuck Norris can cross a bridge before he comes to it.",
		"Chuck Norris is so fast he can be in all the time zones at the same time.",
		"Fear isn't the only emotion Chuck Norris can smell. He can also detect hope, as in \"I hope I don't get a roundhouse kick from Chuck Norris.\"",
		"You would never get tired of Chuck Norris's beard, but it may get tired of you.",
		"If Chuck Norris were the villain in a video game, you'd never win. But if he were the hero, the game would be unplayable because no one controls Chuck Norris.",
		"It is better to correct your own faults than those of another--unless you're Chuck Norris.",
		"Chuck Norris never has to say \"I'll be back\" because Chuck Norris always handles things the first time.",
		"Chuck Norris doesn't have an easy chair. He likes to do everything the hard way.",
		"You can't take a picture of Chuck Norris because nobody can take anything from Chuck Norris.",
		"Chuck Norris can do pushups with his beard.",
		"Chuck Norris can make a snowman out of rain.",
		"Chuck Norris doesn't lose his wallet, but his wallet sometimes runs away from him.",
		"Chuck Norris was HD ready before anybody even knew what HD was.",
		"Chuck Norris pushes the pedal _through_ the metal.",
		"Chuck Norris once had an emotional breakdown. Emotion begged for mercy.",
		"Chuck Norris can measure his weight with a ruler.",
		"Chuck Norris once jumped into a pit of lava on a bet. The lava burned to death.",
		"Chuck Norris never loses weight. He knows exactly where it is.",
		"Chuck Norris was the only kid in his kindergarten class with a full beard.",
		"Disaster flirts with Chuck Norris.",
		"Chuck Norris likes to fly a kite in a monsoon--without a string.",
		"Chuck Norris can rewind a DVD.",
		"Life insurance costs are based on how far you live from Chuck Norris.",
		"Chuck Norris won a staring contest with his reflection.",
		"Ghosts tell Chuck Norris stories around the campfire",
		"Chuck Norris once caught a train--with his bare hands.",
		"After Chuck Norris was born, he drove his mother home from the hospital.",
		"Chuck Norris went as himself for Halloween. There were no survivors.",
		"Chuck Norris knits sweaters out of steel wool.",
		"Chuck Norris never uses a navigation system. The direction he's heading is always the right direction.",
		"Chuck Norris can climb a tree without using his hands.",
		"Chuck Norris knows what's behind the border of the universe.",
		"Chuck Norris can go from zero to sixty in 0.2 seconds, on a bicycle, backwards, blindfolded, in his sleep.",
		"Chuck Norris doesn't vote. He chooses.",
		"Chuck Norris can set water on fire. He can also set fire on water.",
		"Chuck Norris doesn't dot his i's or cross his t's; they do it themselves out of fear.",
		"Genies don't grant wishes to Chuck Norris. Chuck Norris grants wishes to genies.",
		"Chuck Norris's favorite game is winning.",
		"When Chuck Norris is done working out, his gym equipment need ice packs and pain reliever.",
		"Chuck Norris never has to catch his breath. His breath has to catch him.",
		"When Chuck Norris sets his watch, he sets time itself.",
		"When Chuck Norris burns calories, he uses a flamethrower.",
		"Chuck Norris is colder than absolute zero.",
		"Chuck Norris always wins, even when he loses -- which is never.",
		"Chuck Norris didn't just hit puberty; he beat the heck out of it.",
		"Chuck Norris is never faced with danger. Danger is faced with Chuck Norris.",
		"Chuck Norris can turn a black hole white.",
		"Chuck Norris can make a pile of quicksand into slow sand.",
		"Chuck Norris is so fast that when he runs, he can see his back.",
		"Stop signs were invented to keep you out of Chuck Norris's way.",
		"Chuck Norris once roundhouse kicked a revolving door and created a vortex.",
		"Chuck Norris can't cry; his tear ducts are too muscular.",
		"Chuck Norris threw a paper airplane when he was four years old. It landed yesterday.",
		"When Chuck Norris wants to build a fire, he punches a tree and it chops itself into firewood.",
		"The answer is always C -- C as in \"Chuck Norris\".",
		"Chuck Norris can find the end of a circle.",
		"Chuck Norris wins games even when he isn't playing them.",
		"The only reason you're conscious right now is that Chuck Norris doesn't feel like carrying you.",
		"Your keyboard doesn't have keys. Chuck Norris has the keys.",
		"Chuck Norris doesn't need your respect--but he has it anyway.",
		"Chuck Norris can surf with a skateboard and skate with a surfboard.",
		"Chuck Norris kicked the world once, and it hasn't stopped spinning since.",
		"Chuck Norris is so powerful that when you say his name, you get chills.",
		"You can't share your germs with Chuck Norris. Chuck Norris doesn't share.",
		"Chuck Norris special orders pencils without erasers because Chuck Norris doesn't make mistakes.",
		"Before crossing a bridge, trolls look under it for Chuck Norris.",
		"If nobody is perfect, Chuck Norris must be nobody.",
		"Chuck Norris can fly a ship and sail an airplane.",
		"Chuck Norris's mood ring doesn't say whether he's happy or sad. It just says he's Chuck Norris.",
		"Chuck Norris can fix a car's engine through the exhaust pipe.",
		"Time and tide wait for no one-except Chuck Norris.",
		"Pilots have to clear their takeoffs with Chuck Norris.",
		"Chuck Norris can read an eye chart with his eyes closed.",
		"Chuck Norris decides whether Santa Claus has been naughty or nice.",
		"Santa Claus sees you when you're sleeping, but Chuck Norris can see you while Santa is sleeping.",
		"Chuck Norris can't hit a target on the broad side of a barn. Every time he tries, the whole barn falls down.",
		"When Chuck Norris folds during a round of poker, he still wins the pot.",
		"Every dog has its day, except Chuck Norris's dog. It gets a week.",
		"Chuck Norris can catch a falling star and put it in his pocket.",
		"Chuck Norris is awake 25 hours a day.",
	}
	rand.Seed(time.Now().Unix())
	return facts[rand.Intn(len(facts))]
}

func coinFlip() string {
	results := []string{
		"heads",
		"tails",
	}

	rand.Seed(time.Now().UnixNano())
	return results[rand.Intn(len(results))]
}
