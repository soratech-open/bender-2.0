package secrets

import (
	"encoding/json"
	"os"
)

// Secret represents all the secret data needed for this app.
type Secret struct {
	AppID         string `json:"appID"`
	ClientID      string `json:"clientID"`
	ClientSecret  string `json:"clientSecret"`
	SigningSecret string `json:"signingSecret"`
	Token         string `json:"token"`
	OauthToken    string `json:"oAuthToken"`
	Sora          sora   `json:"sora"`
}

type sora struct {
	OfficeWifi          string `json:"officeWifi"`
	GuestWifi           string `json:"guestWifi"`
	BTAPassword         string `json:"btaPassword"`
	LOCPassword         string `json:"locPassword"`
	StatusboardPassword string `json:"statusboardPassword"`
}

// GetSecrets gets secret data from the given file.
func GetSecrets() (Secret, error) {
	serverEnv := os.Getenv("SERVER_ENVIRONMENT")
	var filename string
	var secrets Secret

	switch serverEnv {
	case "development":
		filename = "/var/bender/config/production.json"
	case "production":
		filename = "/var/bender/config/production.json"
	}

	file, err := os.Open(filename)

	if err != nil {
		return secrets, err
	}
	decoder := json.NewDecoder(file)
	err = decoder.Decode(&secrets)

	return secrets, err
}
