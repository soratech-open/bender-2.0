package main

import (
	"encoding/json"
	"io/ioutil"
	"net/http"

	"gitlab.com/soratech/Gamification/bender-2.0/networking"
	"gitlab.com/soratech/Gamification/bender-2.0/secrets"
	"gitlab.com/soratech/Gamification/bender-2.0/slack"
)

// https://bender.soratech.com/api/events

func main() {
	http.HandleFunc("/", index)
	http.HandleFunc("/api/events", slackListener)

	http.ListenAndServe(":80", nil)
}

func index(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("Bite my shiny metal ass"))
}

func slackListener(w http.ResponseWriter, r *http.Request) {
	secret, err := secrets.GetSecrets()

	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	switch r.Method {
	case http.MethodPost:
		// handle requests here
		body, err := ioutil.ReadAll(r.Body)

		if err != nil {
			panic(err)
		}

		var slackReq slack.Request

		err = json.Unmarshal(body, &slackReq)

		if err != nil {
			panic(err)
		}

		switch slackReq.Type {
		case "url_verification":
			if slackReq.Token == secret.Token {

				type urlVerification struct {
					Challenge string `json:"challenge"`
				}

				verification := urlVerification{Challenge: slackReq.Challenge}

				networking.WriteJSON(w, verification)
				return
			}
			w.WriteHeader(http.StatusBadRequest)
		default:
			slack.HandleEvent(slackReq)
			w.WriteHeader(http.StatusOK)
			return
		}
	case http.MethodOptions:
		w.WriteHeader(http.StatusOK)
	default:
		w.WriteHeader(http.StatusMethodNotAllowed)
	}
}
